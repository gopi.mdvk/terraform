provider "aws"{
    region = var.region
    profile = var.profile
}

variable region {}
variable profile{}
variable vpc_cidr{}
variable subnet_cidr{}
variable env {}
variable avail_zone{}
#variable public_key {}
variable keyname {}
variable instance_type {}
variable key_path {}
variable privatekey_path {}


resource "aws_vpc" "demo-vpc"{
    cidr_block = var.vpc_cidr
    tags = {
        Name = "${var.env}-vpc"
    }
}

resource "aws_subnet" "demo-subnet"{
    vpc_id = aws_vpc.demo-vpc.id
    cidr_block = var.subnet_cidr
    availability_zone = var.avail_zone
    tags = {
        Name = "${var.env}-subnet"
    }
}
/*
resource "aws_security_group" "demo-sg"{
    name = "demo-sg"
    vpc_id=aws_vpc.demo-vpc.id

    ingress {
    description      = "ssh login"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]

    }
    ingress {
    description      = "http request"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]

    }
    ingress {
    description      = "https request"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]

    }
    egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    }
    tags ={
        Name = "$var.env}-sg"
    }
}*/

resource "aws_internet_gateway" "demo-igw"{

    vpc_id = aws_vpc.demo-vpc.id
    tags = {
        Name = "$var.env}-igw"
    }
}
/*
resource "aws_route_table" "demo-rt"{
     vpc_id = aws_vpc.demo-vpc.id
     route {
         cidr_block = "0.0.0.0/0"
         gateway_id = aws_internet_gateway.demo-igw.id
     }
}

resource "aws_route_table_association" "subnet-association"{
    subnet_id = aws_subnet.demo-subnet.id
    route_table_id = aws_route_table.demo-rt.id
}*/


resource "aws_default_security_group" "demo-sg"{
    vpc_id = aws_vpc.demo-vpc.id
    ingress {
    description      = "ssh login"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]

    }
    ingress {
    description      = "http request"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]

    }
    ingress {
    description      = "https request"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]

    }
    egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    }
    tags ={
        Name = "{$var.env}-sg"
    }
}

resource "aws_default_route_table" "demo-rt" {
   default_route_table_id = aws_vpc.demo-vpc.default_route_table_id
   route {
       cidr_block = "0.0.0.0/0"
       gateway_id = aws_internet_gateway.demo-igw.id
         }

         tags = {
             Name = "${var.env}-rt"
         }
}
resource "aws_route_table_association" "subnet-association"{
    subnet_id = aws_subnet.demo-subnet.id
    route_table_id = aws_vpc.demo-vpc.default_route_table_id
}

data "aws_ami" "amzon-linux" {
    most_recent = true
    owners = ["amazon"]
    filter {
        name = "name"
        values =["amzn2-ami-kernel-5.10-hvm-*-x86_64-gp2"]
    }
    filter {
        name = "virtualization-type"
        values = ["hvm"]
    }
}

output "my_ami" {
    value = data.aws_ami.amzon-linux.id
}

resource "aws_key_pair" "my-key" {
    key_name = var.keyname
    public_key = file(var.key_path)
}

resource "aws_instance" "demo-ec2"{

    ami = data.aws_ami.amzon-linux.id
    instance_type = var.instance_type
    key_name = aws_key_pair.my-key.key_name
    associate_public_ip_address = true
    subnet_id = aws_subnet.demo-subnet.id
    vpc_security_group_ids = [aws_default_security_group.demo-sg.id]
    availability_zone = var.avail_zone
    tags = {
        Name = "${var.env}-instances"
    }
    #user_data = file("docker.sh") 
}





output "server-ip" {
    value = aws_instance.demo-ec2.public_ip
}


